*;=====================================================
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;=====================================================


;--------------------------------------------------------------------;
; Shared
;--------------------------------------------------------------------;

[colors]
accent = #fba922
background = 001f33
background-alt = #001f33
foreground = #dfdfdf
foreground-alt = #555
primary = #0099ff

;--------------------------------------------------------------------;
; Top panel
;--------------------------------------------------------------------;

[bar/top]
monitor = ${env:MONITOR:eDP1}
width = 100%
height = 24
radius = 0
fixed-center = false
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 2
border-size = 0
border-color =
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2
font-0 = DejaVu Sans:size=9
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = siji:pixelsize=9;1
font-3 = "Font Awesome 5 Free:style=Regular:pixelsize=9;1"
font-4 = "Font Awesome 5 Free:style=Solid:pixelsize=9;1"
font-5 = "Font Awesome 5 Brands:pixelsize=9;1"
modules-left = i3 xwindow
modules-center = nsu nsd pub-ip
modules-right = xbacklight volume battery date
wm-restack = i3
cursor-click = pointer
cursor-scroll = ns-resize

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index% %icon%
label-focused-background = ${colors.background-alt}
label-focused-underline = ${colors.primary}
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %index% %icon%
label-unfocused-padding = 2

; visible = Active workspace on unfocused monitor
label-visible = %index% %icon%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index% %icon%
label-urgent-background = ${colors.primary}
label-urgent-padding = 2

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;
ws-icon-5 = 6;
ws-icon-6 = 7;
ws-icon-7 = 8;
ws-icon-8 = 9;
ws-icon-9 = 10;

[module/xwindow]
type = internal/xwindow

; Available tags:
;   <label> (default)
format = <label>
format-padding = 4

; Available tokens:
;   %title%
; Default: %title%
label = %title%
label-maxlen = 72

; Used instead of label when there is no window title
; Available tokens:
;   None
label-empty = Vide
label-empty-foreground = #707880


[module/xbacklight]
type = internal/xbacklight

format = <ramp><label>
format-underline = ${colors.primary}

ramp-0 = "🌕 "
ramp-1 = "🌔 "
ramp-2 = "🌓 "
ramp-3 = "🌒 "
ramp-4 = "🌑 "

[module/volume]
type = internal/alsa

format-volume = <ramp-volume><label-volume>
label-volume = %percentage%%
label-volume-foreground = ${root.foreground}
label-volume-underline = ${colors.primary}

format-muted-prefix = " "
format-muted-foreground = ${colors.foreground}
format-muted-underline = ${colors.primary}
label-muted = "Muted"

ramp-volume-3 = " "
ramp-volume-2 = " "
ramp-volume-1 = " "
ramp-volume-0 = " "

ramp-volume-underline = ${colors.primary}

[module/battery]
type = internal/battery
adapter = AC0
full-at = 100

format-charging = <animation-charging> <label-charging>
format-charging-underline = ${colors.primary}
battery = BAT1

format-discharging = <ramp-capacity> <label-discharging>
format-discharging-underline = ${self.format-charging-underline}

format-full-prefix = 
format-full-prefix-foreground = ${colors.foreground-alt}
format-full-underline = ${self.format-charging-underline}
ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-foreground = ${colors.foreground}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-foreground = ${colors.foreground}
animation-charging-framerate = 750

[module/date]
type = internal/date
interval = 5

date-alt =
date = " %a %d %b "

time = "  %H:%M"
time-alt =

;format-prefix = 
format-prefix =
format-prefix-foreground = ${colors.foreground}
format-foreground = ${colors.foreground}
format-underline = ${colors.primary}

label = %date% %time%

[module/pub-ip]
type = custom/script
exec = /home/titouan/.config/polybar/pub-ip.sh
interval = 100
label = %output%

[module/nsu]
type = internal/network
interface = wlp2s0
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #FE522C

[module/nsd]
type = internal/network
interface = wlp2s0
label-connected = "%downspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #52FE2C



;--------------------------------------------------------------------;
; Bottom panel
;--------------------------------------------------------------------;

[bar/bottom]
monitor = ${env:MONITOR:eDP1}
bottom = true
width = 100%
height = 24
radius = 0
fixed-center = false
background = #222222
foreground = #fefefe
line-size = 2
border-size = 0
border-color = #000
padding-left = 0
padding-right = 0
module-margin-left = 1
module-margin-right = 1
font-0 = DejaVu Sans:size=9
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = siji:pixelsize=9;1
font-3 = "Font Awesome 5 Free:style=Regular:pixelsize=9;1"
font-4 = "Font Awesome 5 Free:style=Solid:pixelsize=9;1"
font-5 = "Font Awesome 5 Brands:pixelsize=9;1"
modules-left = powermenu memory cpu
modules-center = mpd
modules-right =
tray-position = right
cursor-click = pointer
cursor-scroll = ns-resize
enable-ipc = true

[module/memory]
type = internal/memory
interval = 0.2
format = <label> <bar-used>

label = RAM
label-font = 3
label-foreground = #999

bar-used-width = 30
bar-used-gradient = true
bar-used-foreground-0 = #449f3d
bar-used-foreground-1 = #2f8419
bar-used-foreground-2 = #f5a70a
bar-used-foreground-3 = #ed5456
bar-used-indicator = █
bar-used-indicator-font = 5
bar-used-indicator-foreground = #fff
bar-used-fill = █
bar-used-fill-font = 5
bar-used-empty = █
bar-used-empty-font = 5
bar-used-empty-foreground = #444

[module/cpu]
type = internal/cpu
interval = 1.5

format = <label> <ramp-coreload>

label = CPU
label-font = 3
label-foreground = #999

ramp-coreload-0 = ▁
ramp-coreload-0-font = 2
ramp-coreload-0-foreground = #aaff77
ramp-coreload-1 = ▂
ramp-coreload-1-font = 2
ramp-coreload-1-foreground = #aaff77
ramp-coreload-2 = ▃
ramp-coreload-2-font = 2
ramp-coreload-2-foreground = #aaff77
ramp-coreload-3 = ▄
ramp-coreload-3-font = 2
ramp-coreload-3-foreground = #aaff77
ramp-coreload-4 = ▅
ramp-coreload-4-font = 2
ramp-coreload-4-foreground = #fba922
ramp-coreload-5 = ▆
ramp-coreload-5-font = 2
ramp-coreload-5-foreground = #fba922
ramp-coreload-6 = ▇
ramp-coreload-6-font = 2
ramp-coreload-6-foreground = #ff5555
ramp-coreload-7 = █
ramp-coreload-7-font = 2
ramp-coreload-7-foreground = #ff5555

[module/mpd]
type = internal/mpd
format-online = <bar-progress>  <icon-prev> <icon-stop> <toggle> <icon-next> <icon-random> <icon-repeat> <icon-single> <label-song>
format-offline = <label-offline>
label-offline = mpd is off

icon-play = 
icon-pause = 
icon-stop = 
icon-prev = 
icon-next = 
icon-random = 
icon-repeat = 
icon-single = 

toggle-on-foreground = #fff
toggle-off-foreground = #555

bar-progress-width = 30
bar-progress-format = %{+o +u}%fill%%{-o -u}%indicator%%{+o +u}%empty%%{-u -o}
; bar-progress-indicator = |
bar-progress-indicator = █
bar-progress-indicator-foreground = #fff
bar-progress-indicator-font = 3
; bar-progress-fill = ─
bar-progress-fill = █
bar-progress-fill-foreground = #bbb
bar-progress-fill-font = 3
; bar-progress-empty = ─
bar-progress-empty = ▒
bar-progress-empty-font = 3
bar-progress-empty-foreground = #000


[module/powermenu]
type = custom/menu

format-padding = 3
format-foreground = #fff

label-open = 
label-close = 
label-separator = " • "

menu-0-0 = 
menu-0-0-foreground = $R{colors.primary}
menu-0-0-exec = i3-msg exit
menu-0-1 = 
menu-0-1-foreground = ${colors.primary}
menu-0-1-exec = menu-open-1
menu-0-2 = 
menu-0-2-foreground = ${colors.primary}
menu-0-2-exec = menu-open-2

menu-1-0 = Cancel
menu-1-0-foreground = ${colors.accent}
menu-1-0-exec = menu-open-0
menu-1-1 = Reboot
menu-1-1-foreground = ${colors.accent}
menu-1-1-exec = reboot

menu-2-0 = Power off
menu-2-0-foreground = ${colors.accent}
menu-2-0-exec = poweroff
menu-2-1 = Cancel
menu-2-1-foreground = ${colors.accent}
menu-2-1-exec = menu-open-0

;=======================
; GLOBAL ===============
;=======================

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 5
margin-bottom = 5
