#request mod circle

/* Window hints */
#request setfloating  false
#request setdecorated true
#request setfocused   false
#request setmaximized false

#request setopacity "native"

#request setmirror false

#request setversion 3 3
#request setshaderversion 330

#request settitle "GLava"

#request setgeometry 0 0 600 400

#request setbg 00000000

#request setxwintype "normal"

#request setclickthrough false

#request setsource "auto"

#request setswap 1

#request setinterpolate true

#request setframerate 0

#request setfullscreencheck false

#request setprintframes true

#request setsamplesize 1024

#request setbufsize 4096

#request setsamplerate 22050

#request setforcegeometry false

#request setforceraised false

#request setbufscale 1
