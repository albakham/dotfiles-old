#export TERM="xterm-256color"
#source ~/.custom

if [[ "$TERM" == "xterm-256color" ]];
 then
  source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
 else
  bash
fi

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


### Colorization functions
function _colorman() {
  env \
      LESS_TERMCAP_mb=$(printf "\e[1;35m") \
      LESS_TERMCAP_md=$(printf "\e[1;34m") \
      LESS_TERMCAP_me=$(printf "\e[0m") \
      LESS_TERMCAP_se=$(printf "\e[0m") \
      LESS_TERMCAP_so=$(printf "\e[7;40m") \
      LESS_TERMCAP_ue=$(printf "\e[0m") \
      LESS_TERMCAP_us=$(printf "\e[1;33m") \
      "$@"
}
function man() { _colorman man "$@"; }
function perldoc() { command perldoc -n less "$@" |man -l -; }

### Alias
alias free='LC_ALL=C free'
alias config='/usr/bin/git --git-dir=$HOME/.myconf/ --work-tree=$HOME'
alias monip="python2 ~/.monip.py"
alias rm='rm -i'
alias lolls='ls -l|lolcat'
# Move standard ls
alias ols="/usr/bin/ls --color=always"
# Base formats
alias ls="colorls -A"           # short, multi-line
alias ll="colorls -1A"          # list, 1 per line
alias ld="ll"                   # ^^^, NOTE: Trying to move to this for alternate hand commands
alias la="colorls -lA"          # list w/ info
alias ltree="colorls --tree=2"
# [d] Sort output with directories first
alias lsd="ls --sort-dirs"
alias lld="ll --sort-dirs"
alias ldd="ld --sort-dirs"
alias lad="la --sort-dirs"
# [t] Sort output with recent modified first
alias lst="ls -t"
alias llt="ll -t"
alias ldt="ld -t"
alias lat="la -t"
# [g] Add git status of each item in output
alias lsg="ls --git-status"
alias llg="ll --git-status"
alias ldg="ld --git-status"
alias lag="la --git-status"
# Grep options
GREP_OPTS='--color=auto'      # for aliases since $GREP_OPTIONS is deprecated
GREP_COLOR='1;32'             # (legacy) bright green rather than default red
GREP_COLORS="ms=$GREP_COLOR"  # (new) Matching text in Selected line = green
alias  grep='grep $GREP_OPTS'
alias egrep='egrep $GREP_OPTS'
alias fgrep='fgrep $GREP_OPTS'
