export TERM=xterm-color
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

PS1='\e[33;1m\u@\h: \e[31m\W\e[0m\$ '
alias ls="ls --color=always"
